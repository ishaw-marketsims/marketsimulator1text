/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class MarketSim01Text {

    public static void main(String[] args)
    {
        // using this for both buyers and sellers, at least for now
        // possibly will enforce a global max of agents in future but with asymmetric b/s
        final Integer MAX_NUM_AGENTS = 20;
        
        BuyerList buyerList = new BuyerList(MAX_NUM_AGENTS);
        SellerList sellerList = new SellerList(MAX_NUM_AGENTS);
        MarketFacilitator marketFacilitator = new MarketFacilitator(buyerList, sellerList);
        Integer option;
        do {
            System.out.println("Buyer List: \n" + buyerList);
            System.out.println("Seller List: \n" + sellerList);
            System.out.println("0: quit");
            System.out.println("1: add a buyer");
            System.out.println("2: add a seller");
            System.out.println("3: run a negotiation");
            option = Input.getInteger("option: ");
            switch (option) {
                case 0:
                    System.out.println("quitting program");
                    break;
                case 1:
                    Integer MAX_PRICE = Input.getInteger("Max Price: ");
                    Integer demandUnits = Input.getInteger("Units of Demand: ");
                    buyerList.addBuyer(MAX_PRICE, demandUnits);
                    break;
                case 2:
                    Integer MIN_PRICE = Input.getInteger("Min Price: ");
                    Integer supplyUnits = Input.getInteger("Units of Supply: ");
                    sellerList.addSeller(MIN_PRICE, supplyUnits);
                    break;
                case 3:
                    if(buyerList.getNumberOfBuyers() > 0 && sellerList.getNumberOfSellers() > 0){
                        // MarketFacilitator marketFacilitator = new MarketFacilitator(buyerList, sellerList);
                        marketFacilitator.runNegotiation();
                    } else {
                        System.out.println("Need more buyers and sellers.");
                    }
            }
        } while (option!=0);
    }
}
