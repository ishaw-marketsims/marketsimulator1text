/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class BidList {
    private final Bid[] bidList;
    private Integer numberOfBids;
    
    public BidList(Integer bidListSize){
        this.numberOfBids = 0;
        this.bidList = new Bid[bidListSize];
    }
    
    public void addBid(Integer bidPrice, Buyer buyer){
        this.bidList[numberOfBids] = new Bid(bidPrice, buyer);
        this.numberOfBids++;
    }
    
    public Bid getBid(Integer i){
        return this.bidList[i];
    }
    
    public Integer getNumberOfBids(){
        return this.numberOfBids;
    }
    
    public Integer getBidListSize(){
        return bidList.length;
    }
    
}
