/*
 * Copyright (C) 2018 ishaw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author ishaw
 */

public class MarketFacilitator {
    private final Integer LENGTH_OF_NEGOTIATION = 10; // number of steps in the negotiation
    private final BuyerList buyerList;
    private final SellerList sellerList;
    

    public MarketFacilitator(BuyerList buyerList, SellerList sellerList){
        this.buyerList = buyerList;
        this.sellerList = sellerList;
    }
    
    public void createRandomBuyer(){
        buyerList.addBuyer(20 + (int)(Math.random() * 80), 100 + (int)(Math.random() * 200));
    }
  
    public void createRandomSeller(){
        sellerList.addSeller(20 + (int)(Math.random() * 80), 100 + (int)(Math.random() * 200));
    }
  
    public void runNegotiation(){
        BidList bidList = new BidList(buyerList.getNumberOfBuyers());
        System.out.println(bidList.getBidListSize());
        
        // get bids from buyers, populate the bidList
        for(Integer i=0;i<bidList.getBidListSize();i++){
            if(buyerList.getBuyer(i).getDemandUnits() > 0) {//  if the buyer has any demand at all...
                bidList.addBid(buyerList.getBuyer(i).bidPrice(), buyerList.getBuyer(i));
            System.out.println("added bid "+ buyerList.getBuyer(i).bidPrice());}
        }
        System.out.println("Bids ready: numsellers "+sellerList.getNumberOfSellers()+", numbids "+bidList.getNumberOfBids());
        
        // begin the negotiation        
        for(Integer i=0;i<LENGTH_OF_NEGOTIATION;i++){
            System.out.println("step "+i);
            
            // reset the bids to not accepted
            for(Integer bid=0;bid<bidList.getBidListSize();bid++){
                bidList.getBid(bid).setBidAccepted(false);
            
            
            }
            
            
            // each seller looks for a bid it can accept
            // if found, it accepts it, locking it for the rest of the round and fulfils as much demand as it can
            
            for(Integer j=0;j<sellerList.getNumberOfSellers();j++){                                     // for every seller
                if(sellerList.getSeller(j).getSupplyUnits() > 0){                                       // if the seller has supply left to sell
                    for(Integer k=0;k<bidList.getNumberOfBids();k++){                                   // for every bid
                        if(!bidList.getBid(k).getBidAccepted()){                                        // if the bid hasn't already been accepted
                            if(sellerList.getSeller(j).bidPrice() < bidList.getBid(k).getBidPrice()){   // and if the sales price is less than the buyer bid price
                                bidList.getBid(k).setBidAccepted(true);                                 // accept the sale
                                Seller winningSeller = sellerList.getSeller(j);                         // (def to tidy up the code)
                                Buyer winningBuyer = bidList.getBid(k).getBuyer();                      // (def to tidy up the code)
                                Integer winningBidPrice = (winningSeller.bidPrice() + winningBuyer.bidPrice()) / 2;// set the unit price
                                Integer salesVolume;
                                System.out.println("Winning price: "+winningBidPrice);
                                if(winningSeller.bidAmount() <= winningBuyer.bidAmount()){              // if supply is less than or equals demand, sell out and reduce demand
                                    salesVolume = winningSeller.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningBuyer.setDemandUnits(winningBuyer.bidAmount() - winningSeller.bidAmount());
                                    winningSeller.setSupplyUnits(0);
                                } else {                                                                // supply is more than demand, fully supply and reduce available supply left
                                    salesVolume = winningBuyer.bidAmount();
                                    winningSeller.addMoneyMade(winningBidPrice * salesVolume);
                                    winningBuyer.addMoneySpent(winningBidPrice * salesVolume);
                                    winningSeller.setSupplyUnits(winningSeller.bidAmount() - winningBuyer.bidAmount());
                                    winningBuyer.setDemandUnits(0);
                                    // remove bid
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
